from flask import jsonify, Blueprint

print("create api routes")

blueprint = Blueprint("api",__name__,url_prefix="/")

@blueprint.route("/ping",methods=['GET'])
def ping():
    return jsonify("pong")

@blueprint.route("/employees",methods=['GET'])
def employeeList():
    
    employees = [
        {"firstname":"spencer","lastname":"hopkinson"},
        {"firstname":"finlay","lastname":"hopkinson"},
        {"firstname":"noah","lastname":"hopkinson"}        
    ]
    
    return jsonify(employees)