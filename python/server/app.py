from flask import Flask

def create_app(test_config=None):
    app = Flask(__name__)
    
    if test_config is None:
        #load the instance config
        pass
    else:
        app.config.update(test_config)
    
    #Attach logging middleware
    #app.wsgi_app = HttpLogger(app.wsgi_app)
    
    #Setup views
    #from server import views
    #app.register_blueprint(views.blueprint)

    #Setup api routes
    import api
    app.register_blueprint(api.blueprint)
    
    #run the app from py server.py
    #app.run()
    return app