﻿#Create a sprint boot app

Clear-Host

if(Test-Path "./spring-boot")
{
    Remove-Item "./spring-boot" -Recurse    
}

$data = @{
    language="java"
    bootVersion="2.5.6"
    dependencies="web"
    packageName="com.apisoup.java"
    name="spring-boot"
    type="maven-project"
    groupId="com.apisoup.java"
    description="java api for apisoup"
}

Remove-Item "spring-boot.zip"

Invoke-WebRequest "https://start.spring.io/starter.zip" `
    -OutFile "spring-boot.zip" `
    -Body $data `
    -ContentType "application/x-www-form-urlencoded" `
    -Method Post
    
Expand-Archive "spring-boot.zip"

#https://start.spring.io/starter.zip?type=maven-project&language=java&bootVersion=2.5.6&baseDir=spring-boot&groupId=com.example&artifactId=spring-boot&name=spring-boot&description=Demo project for Spring Boot&packageName=com.apisoup.java&packaging=jar&javaVersion=11&dependencies=web