#used to generate the project structure

Clear-Host

Remove-Item "./apisoup" -Recurse

mvn archetype:generate "-DgroupId=com.apisoup.java" `
    "-DartifactId=apisoup" `
    "-DarchetypeArtifactId=maven-archetype-quickstart" `
    "-DarchetypeVersion=1.4" `
    "-DinteractiveMode=false" `
    "-DgeneratePom=true"