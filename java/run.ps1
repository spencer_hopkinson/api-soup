param($jar="apisoup-1.0.0-jar-with-dependencies.jar",$useJar=$false)

Clear-Host

mvn clean package

if($useJar){    
    
    Write-Host "Attempting to run $($jar)" -ForegroundColor Yellow

    Start-Sleep -Seconds 2
    
    #java -jar "./target/api/$($jar)"    
    $count = 1
    while(!(Test-Path -Path "./target/$($jar)")){
        Write-Host "Waiting for jar to appear $($count)..." -ForegroundColor Yellow
        Start-Sleep -Seconds 1
        
        if($count -ge 30){
            Write-Host "I give up!" -ForegroundColor Red
            exit
        }
        
        $count++
    }

    $process = Start-Process -FilePath "java" `
            -ArgumentList "-jar ./target/$($jar)" `
            -PassThru
}
else{
    #java -cp ".\target\dependency\*;.\target\classes\" com.apisoup.java.App

    $process = Start-Process -FilePath "java" `
                -ArgumentList "-cp .\target\dependency\*;.\target\classes\ com.apisoup.java.App" `
                -PassThru
}

Invoke-WebRequest "http://localhost:4567/hello" -UseBasicParsing

Start-Sleep -Seconds 5

Stop-Process -Id $process.Id

Set-Location ".."