﻿param($subject="localhost")

Clear-Host

openssl req -x509 -nodes -newkey rsa:4096 -keyout "$($subject).key" -out "$($subject).pem" -days 365 -subj "/CN=$($subject)"