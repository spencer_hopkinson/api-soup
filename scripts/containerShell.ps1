param(
[Parameter(Mandatory=$true)]
$container=""
)

# run a bash prompt on an already running container

docker exec -it $container bash