﻿Clear-Host

$version = "1.20.1"
$uri = "https://nginx.org/download/nginx-$($version).zip"

if((Test-Path -Path "nginx-$($version).zip") -eq $false)
{
    Write-Host "Downloading $($uri)" -ForegroundColor Yellow
    Invoke-webrequest -Uri $uri -OutFile "nginx-$($version).zip"
}

Write-Host "Unzipping nginx" -ForegroundColor Yellow

Expand-Archive "nginx-$($version).zip" -DestinationPath nginx

Copy-Item ./nginx/nginx-$($version) -Destination c:\nginx -Force -Recurse

Remove-Item -Path ./nginx -Recurse