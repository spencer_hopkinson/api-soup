﻿Clear-Host

$uri = "https://slproweb.com/download/Win64OpenSSL_Light-1_1_1k.msi"

if((Test-Path -Path "Win64OpenSSL_Light-1_1_1k.msi") -eq $false)
{
    Write-Host "Downloading $($uri)" -ForegroundColor Yellow
    Invoke-webrequest -Uri $uri -OutFile "Win64OpenSSL_Light-1_1_1k.msi"
}

Write-Host "Installing open ssl" -ForegroundColor Yellow

./Win64OpenSSL_Light-1_1_1k.msi