Clear-Host

docker compose -f ../docker-compose.yml up --detach

Write-Host "Sleeping for a bit..." -ForegroundColor Yellow
Start-Sleep -Seconds 5

#Python API
Write-Host "Testing Python API..." -ForegroundColor Yellow
Invoke-WebRequest -uri http://localhost:8080/ping -UseBasicParsing

#JS API
Write-Host "Testing JS API..."  -ForegroundColor Yellow
Invoke-WebRequest http://localhost:8081/ping -UseBasicParsing

#Go API
Write-Host "Testing Go API..."  -ForegroundColor Yellow
Invoke-WebRequest http://localhost:8082/ping -UseBasicParsing

#Java API
Write-Host "Testing Java API..."  -ForegroundColor Yellow
Invoke-WebRequest http://localhost:8083/hello -UseBasicParsing