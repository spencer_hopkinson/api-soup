﻿param($version = "3.8.6")

if(!(Test-Path -Path "maven-$($version).zip"))
{
    #Invoke-WebRequest "https://dlcdn.apache.org/maven/maven-3/$($version)/binaries/apache-maven-$($version)-bin.zip" `
    #    -OutFile "maven-$($version).zip"

    Invoke-WebRequest "https://archive.apache.org/dist/maven/maven-3/$version/binaries/apache-maven-$version-bin.zip " `
        -OutFile "maven-$($version).zip"
}

if(!(Test-Path -Path "c:\maven"))
{
    New-Item -Path "c:\maven" -ItemType Directory
    Expand-Archive "maven-$version.zip" -DestinationPath "c:\maven"
}