package main

import (
    "github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/ping", ping)
	router.Run(":8082")
}

func ping(c *gin.Context){
	c.JSON(200, gin.H{
		"message": "pong",
	})
}