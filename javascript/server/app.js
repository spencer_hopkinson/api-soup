const express = require('express');
const app = express();

const port = process.env.PORT || 8081

app.get('/ping', (req, res)=>{
    res.json({ping:"pong"});
})

app.get('/info', (req, res)=>{
    res.json({ version: process.env.npm_package_version });
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
